import pandas as pd
import numpy as np
from sklearn.manifold import TSNE
from gensim import corpora, models
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import re
from bokeh.plotting import figure, show
from bokeh.models import HoverTool, CustomJS, ColumnDataSource, Slider
from bokeh.layouts import column
from bokeh.palettes import all_palettes
import csv

df = pd.read_csv('data/eastenders_pulse_dataset.csv',encoding="iso-8859-1",delimiter=",",
                 quoting=csv.QUOTE_MINIMAL)

df = df[:200]

features_list = df.columns[1:]

## nlp preprocessing
# Removing numerals:
df['comment_tokens'] = df.comment.map(lambda x: re.sub(r'\d+', '', x))
# Lower case:
df['comment_tokens'] = df.comment_tokens.map(lambda x: x.lower())
# tokenisation
df['comment_tokens'] = df.comment_tokens.map(lambda x: RegexpTokenizer(r'\w+').tokenize(x))
# remove stopword
stop_en = stopwords.words('english')
df['comment_tokens'] = df.comment_tokens.map(lambda x: [t for t in x if t not in stop_en])
# remove short word
df['comment_tokens'] = df.comment_tokens.map(lambda x: [t for t in x if len(t) > 1])


# create lda model
np.random.seed(2017)
NUM_TOPICS = 6
texts = df['comment_tokens'].values
dictionary = corpora.Dictionary(texts)
corpus = [dictionary.doc2bow(text) for text in texts]
ldamodel = models.ldamodel.LdaModel(corpus, id2word=dictionary,
                                    num_topics=NUM_TOPICS, passes=5, iterations=50,  minimum_probability=0)

# print topics
for idx in range(NUM_TOPICS):
    # Print the first 10 most representative topics
    print("Topic #%s:" % idx, ldamodel.print_topic(idx, 10))

# Refactoring results of LDA into numpy matrix (num of docs x number_of_topics).
hm = np.array([[y for (x,y) in ldamodel[corpus[i]]] for i in range(len(corpus))])

# find topic string and add column to df
topics =[]
for arr in hm:
    corresponding_topics = [np.argmax(arr)]
    topic_id = max(set(corresponding_topics), key=corresponding_topics.count)  # select the most common topic
    topic = [i[0] for i in ldamodel.show_topic(topic_id,topn=5)]
    topic_str = ' '.join(str(e) for e in topic)
    topics.append(topic_str)

df["topic"] = topics

# reduce dimensionality using t-SNE algorithm:
tsne = TSNE(random_state=2017, perplexity=30)
tsne_embedding = tsne.fit_transform(hm)
tsne_embedding = pd.DataFrame(tsne_embedding, columns=['x','y'])
tsne_embedding['hue'] = hm.argmax(axis=1)


# Plot results with Bokeh for scatter plot with interactions.
source = ColumnDataSource(
        data=dict(
            x = tsne_embedding.x,
            y = tsne_embedding.y,
            colors = [all_palettes['Set1'][8][i] for i in tsne_embedding.hue],
            title = df.comment,
            date = df.date,
            topic = df.topic,
            ai_score = df.ai_score,
            alpha = [0.9] * tsne_embedding.shape[0],
            size = [7] * tsne_embedding.shape[0]
        )
    )


hover_tsne = HoverTool(names=["df"], tooltips="""
    <div style="margin: 10">
        <div style="margin: 0 auto; width:300px;">
            <span style="font-size: 12px; font-weight: bold;">Comment:</span>
            <span style="font-size: 12px">@title</span>
            <span style="font-size: 12px; font-weight: bold;">Date:</span>
            <span style="font-size: 12px">@date</span>
            <span style="font-size: 12px; font-weight: bold;">AI Score:</span>
            <span style="font-size: 12px">@ai_score</span>
            <span style="font-size: 12px; font-weight: bold;">Topic:</span>
            <span style="font-size: 12px">@topic</span>
        </div>
    </div>
    """)

tools_tsne = [hover_tsne, 'pan', 'wheel_zoom', 'reset']
plot_tsne = figure(plot_width=800, plot_height=800, tools=tools_tsne, title='Comments')

plot_tsne.circle('x', 'y', size='size', fill_color='colors',
                 alpha='alpha', line_alpha=0, line_width=0.01, source=source, name="df", legend='topic')

plot_tsne.legend.location = "bottom_left"

callback = CustomJS(args=dict(source=source), code="""
    var data = source.data;
    var f = cb_obj.value
    x = data['x']
    y = data['y']
    colors = data['colors']
    alpha = data['alpha']
    title = data['title']
    date = data['date']
    ai_score = data['ai_score']
    topic = data['topic']
    size = data['size']
    for (i = 0; i < x.length; i++) {
        if (ai_score[i] <= f) {
            alpha[i] = 0.9
            size[i] = 7
        } else {
            alpha[i] = 0.05
            size[i] = 4
        }
    }
    source.trigger('change');
""")

slider = Slider(start=1, end=10, value=10, step=1, title="AI Score")
slider.js_on_change('value', callback)

layout = column(slider, plot_tsne)

show(layout)
