## Text Representation and Classification using TF-IDF and LDA

### TF-IDF

The first script provides a way to vizualise the most important word using tfidf.
The output is a wordcloud allowing the vizualisation of drivers terms for each programme.

See the notebook [here](notebook_word_cloud.ipynb)


### LDA Topic Modeling

The second script provides a way to compute a LDA analysis using Gensim and
t-SNE to vizualise the output using dimension reduction.

See the notebook [here](notebook_topic_modeling_viz.ipynb)

