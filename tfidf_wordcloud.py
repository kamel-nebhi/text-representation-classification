import pandas as pd
import csv
from nltk.corpus import stopwords
from nltk import word_tokenize
import string
import re
import gensim
from gensim import corpora
import matplotlib.pyplot as plt
from wordcloud import WordCloud


df = pd.read_csv('data/apprentice_pulse_dataset.csv',encoding="iso-8859-1",delimiter=",",quoting=csv.QUOTE_MINIMAL)
# filter df on ai_score if necessary
#df = df[(df["ai_score"] > 3) & (df["ai_score"] < 7)]
#df = df[(df["ai_score"] > 7)]

features_list = df.columns[1:]
doc_complete = df['comment'].tolist()


# nlp preprocessing
stop = set(stopwords.words('english'))
exclude = set(string.punctuation)


def clean_text(text):
    cleaned_text = []
    if type(text) is str:
        tokenized_text = word_tokenize(text.lower())
        cleaned_text = [t for t in tokenized_text if re.match('[a-zA-Z\-][a-zA-Z\-]{2,}', t)]
    return cleaned_text


tokenized_data = []
for text in doc_complete:
    tokenized_data.append(clean_text(text))

# create a dictionary and corpus for gensim
dictionary = corpora.Dictionary(tokenized_data)
corpus = [dictionary.doc2bow(doc) for doc in tokenized_data]
# create a tfidf model
tfidf_model = gensim.models.tfidfmodel.TfidfModel(corpus, id2word=dictionary)
corpus_tf_idf = tfidf_model[corpus]
d = {dictionary.get(id): value for doc in corpus_tf_idf for id, value in doc}

result = sorted(d.items(), key=lambda t : t[1], reverse=True)

# create a df with results
df_2 = pd.DataFrame.from_records(result,columns=["word","tfidf"])
tfidf_df = df_2[:50]
tfidf_dict = dict(zip(tfidf_df.word, tfidf_df.tfidf))

# plot results in a wordcloud
plt.figure(figsize=(20,10))
plt.imshow(WordCloud(background_color="white", width=1600, height=800).fit_words(tfidf_dict))
plt.axis("off")
plt.title("Highest tfidf", fontsize=20)
plt.savefig("apprentice_word_cloud")
plt.show()

